function helper()
{
    document.getElementById("title").addEventListener("click", myFunction);
    var counter = 0;    //Counter for how many time u got hit

    function myFunction()
    {
        var canvas = document.querySelector('canvas');
        var ctx = canvas.getContext('2d');

        var width = canvas.width = window.innerWidth;
        var height = canvas.height = window.innerHeight;
        //mouse object holding position
        let mouse = 
        {
          x: 20,
          y: height,
        };

        addEventListener("mousemove", function(event)
        {
          mouse.x = event.clientX;
          mouse.y = event.clientY;
        });


        function random(min, max) 
        {
            var num = Math.floor(Math.random() * (max - min + 1)) + min;
            return num;
        }

        function Ball(x, y, velX, velY, color, size)
        {
            this.x = x;
            this.y = y;
            this.velX = velX;
            this.velY = velY;
            this.color = color;
            this.size = size;
        }

        Ball.prototype.draw = function() 
        {
            ctx.beginPath();
            ctx.fillStyle = this.color;
            ctx.arc(this.x, this.y, this.size, 0, 2 * Math.PI);
            ctx.fill();
        }

        Ball.prototype.update = function() 
        {/*
            if ((this.x + this.size) >= width) {
              this.velX = -(this.velX);
            }
          
            if ((this.x - this.size) <= 0) {
              this.velX = -(this.velX);
            }
          
            if ((this.y + this.size) >= height) {
              this.velY = -(this.velY);
            }
          
            if ((this.y - this.size) <= 0) {
              this.velY = -(this.velY);
            }*/
          
            this.x += this.velX;
            this.y += this.velY;
        }
        Ball.prototype.collisionDetection = function()
        {
          for(let j = 0; j < ballArray.length; j++)
          {
            if(!(this === ballArray[j]))
            {
              let dx = ballArray[j].x - ballPlayer.x;
              let dy = ballArray[j].y - ballPlayer.y;

              let distance = Math.sqrt(Math.pow(dx,2) + Math.pow(dy,2));

              if(distance < ballArray[j].size + ballPlayer.size)
              {
                ballPlayer.color =  'rgb(' + random(0,255) + ',' + random(0,255) + ',' + random(0,255) +')';
                counter += 1;
              }

              //Change bombs color
              let distanceX = this.x - ballArray[j].x; 
              let distanceY = this.y - ballArray[j].y;

              let distanceTotal = Math.sqrt(Math.pow(distanceX,2) + Math.pow(distanceY,2));

              if(distanceTotal < this.size + ballArray[j].size)
              {
                ballArray[j].color = this.color = 'rgb(' + random(200,255) + ',' + random(0,150) + ',' + 0 +')';
              }
              //=================================================================================
            }

          }

        }

        //=====================================================================
        var ballArray = [];
        var ballPlayer;
        function init()
        {
          ballPlayer = new Ball(undefined, undefined, 0, 0, "green", 40);
        }
        init();
        //Ball(x, y, velX, velY, color, size)
        while (ballArray.length < 50) 
        {
          var size = random(10,30);
          var ball = new Ball(random(0 + size,width - size),  random(size + size,height*.2),  random(-1,1), random(4,8), 'orangered', size);

          ballArray.push(ball);
        }

        function loop() 
        {
            var img = new Image();
            img.src = "https://db4sgowjqfwig.cloudfront.net/campaigns/110358/assets/472138/eruption_Hotenow.jpg?1434651928"; 
            img.onload = ctx.drawImage(img, 0, 0, width, height);
            ctx.fillStyle = 'rgba(0, 0, 0, 0.25)';
            ctx.fillRect(0, 0, width, height);
          
            for (var i = 0; i < ballArray.length; i++) 
            {
              ballArray[i].draw();
              ballArray[i].collisionDetection();              
              ballArray[i].update();
            }
            ballPlayer.x = mouse.x;
            ballPlayer.y = height;

            ballPlayer.draw();

            requestAnimationFrame(loop);
        }
      loop();
      
      setTimeout(winAlert, 5000);
      function winAlert()
      {
          if(counter == 0)
          {
              alert("Congrats. You were hit " + counter + " times and survived!. Hit 'Refresh to play again!");
          }
          else
          {
              alert("End of Game. You suffered " + counter + " points of damage and died! Hit 'Refresh' to play again!");
          }
          
      }
      
    }
}

helper();



